// Proxy configuration file. See link for more information:
// https://github.com/angular/angular-cli/blob/master/docs/documentation/stories/proxy.md

const PROXY_CONFIG = [{
    context: [
        "/api/v1/host",
    ],
    target: "http://192.168.254.140:8080",
    secure: false
}, {
    context: [
        "/api/v1/exec",
    ],
    target: "http://192.168.254.140:8080",
    secure: false,
    ws: true
},{
    context: [
        "/api/v1/group",
    ],
    target: "http://192.168.254.140:8080",
    secure: false,
},{
    context: [
        "/api/v1/batchcreate",
    ],
    target: "http://192.168.254.140:8080",
    secure: false,
},{
    context:[
        "/api/v1/user",
    ],
    target: "http://192.168.254.140:8080",
    secure: false,
}, {
    context: [
        "/api/v1/getuser"
    ],
    target: "http://192.168.254.140:8080",
    secure: false,
}, {
    context: ["/api/v1/cve"],
    target: "http://192.168.254.140:8080",
    secure: false,
}, {
    context: [
        "/api/v1/history"
    ],
    target: "http://192.168.254.140:8080",
    secure: false
},]

module.exports = PROXY_CONFIG;
package web

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	genericStorage "gitlab.com/ivyent/panther/pkg/storage/generic"
)

func (in *Handler) getUser(w http.ResponseWriter, r *http.Request) {
	defer in.logger.Sync()
	defer func() {
		if rec := recover(); rec != nil {
			in.finalizeError(w, fmt.Errorf("Internal Server Error"), http.StatusInternalServerError)
			in.logger.Error(rec)
		}
	}()
	defer in.finalizeHeader(w)
	switch r.Method {
	case "GET":
		targets := strings.Split(r.URL.Query().Get("search"), ",")
		if len(targets) == 1 && targets[0] == "" {
			in.finalizeError(w, fmt.Errorf("Target required"), http.StatusBadRequest)
			in.logger.Errorf("No target was specified during query")
			return
		}
		in.logger.Debugf("Search user: %s", strings.Join(targets, ", "))
		var all bool
		for _, each := range targets {
			if each == "*" {
				all = true
				break
			}
		}
		sortor := genericStorage.NewSortor()
		if all {
			cv := genericStorage.NewUserList()
			err := in.storage.List(cv)
			if err != nil {
				in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
				in.logger.Error(err)
				return
			}
			for i := range cv.Members {
				sortor.AppendMember(&cv.Members[i])
			}
		} else {
			for _, each := range targets {
				newObj := genericStorage.NewUser()
				newObj.SetName(each)
				err := in.storage.Get(newObj)
				if err != nil {
					in.logger.Error(err)
					if genericStorage.IsInternalError(err) {
						in.finalizeStorageError(w, err)
						return
					}
					in.finalizeError(w, fmt.Errorf("Database Failure"), http.StatusInternalServerError)
					return
				}
				sortor.AppendMember(newObj)
			}
		}
		dAtA, err := json.Marshal(sortor.OrderByName())
		if err != nil {
			panic(err)
		}
		in.finalizeJSON(w, bytes.NewReader(dAtA))
	default:
		return
	}
}

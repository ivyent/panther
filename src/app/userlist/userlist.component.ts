import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { User, LoginService } from '@core';
import { SelectionModel } from '@angular/cdk/collections';
import { AppUserComponent } from 'app/user/user.component';
import { AppHostConfirmComponent } from 'app/host';
import { Subscription, Observable } from 'rxjs';
import { HeaderService } from 'app/header';
import { sortingDataAccessor, filterAny } from '@core/utils';

@Component({
    selector: 'app-userlist',
    templateUrl: './userlist.component.html',
    styleUrls: ['./userlist.component.scss']
})

export class AppUserListComponent implements OnInit, OnDestroy {
    displayedColumns = [ 'select', 'name', 'metadata.created_at', 'userType', 'view'];
    dataSource: MatTableDataSource<User>;
    selection = new SelectionModel<User>(true, []);

    uPattern = '/^[a-zA-Z0-9_-]{4,16}$/';
    subscription: Subscription;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(
        private dialog: MatDialog,
        private data: LoginService,
        private _header: HeaderService,
    ) {}
    ngOnInit () {
        this._header.title = '用户管理';
        this.subscription = this.data.getUser('*').subscribe((data) => {
            this.dataSource = new MatTableDataSource(data);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sortingDataAccessor = sortingDataAccessor;
            this.dataSource.sort = this.sort;
            this.dataSource.filterPredicate = filterAny;
        });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    search(value: string) {
        this.dataSource.filter = value.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    toggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }
    create() {
        const dialogRef = this.dialog.open(AppUserComponent, {
            width: '360px',
        });
        dialogRef.afterClosed().subscribe(() => {
            this.subscription = this.data.getUser('*').subscribe((data) => {
                this.dataSource = new MatTableDataSource(data);
            });
        });
    }


    deleteSelection() {
        const ref = this.dialog.open(AppHostConfirmComponent, {disableClose: true, data: this.selection.selected});
        ref.afterClosed().subscribe((agreed) => {
            if (!agreed) {
                return;
            }
            const recur = (fn: (number) => Observable<void>, length: number) => {
                const index = length - 1;
                if (index < 0) {
                    this.selection.clear();
                    this.refresh();
                    return;
                }
                fn(length - 1).subscribe(() => {
                    recur(fn, index);
                }, (err) => {
                    console.error(err);
                });
            };
            // Delete machine recursively.
            recur((index: number) => {
                return this.data.delete(this.selection.selected[index].metadata.name);
            }, this.selection.selected.length);
        });
    }
    update(elem) {
        this.dialog.open(AppUserComponent, {
            data: elem,
            width: '360px',
        });
    }
    delete(elem) {
        console.log(elem.name);
        this.dialog.open(AppHostConfirmComponent, {
            data: [elem],
            disableClose: true
        }).afterClosed().subscribe((agreed) => {
            if (!agreed) {
                return;
            }
            this.data.delete(elem.name).subscribe(() => {
                this.subscription = this.data.getUser('*').subscribe((data) => {
                    console.log(data);
                    this.dataSource = new MatTableDataSource(data);
                });
            });
        });

    }

    refresh() {
        if (this.subscription) {
            this.subscription.unsubscribe(); // GC previous thread.
            this.subscription = null;
        }
        this.subscription = this.data.getUser('*').subscribe((data) => {
            this.dataSource.data = data;
                return;
        });
    }
}

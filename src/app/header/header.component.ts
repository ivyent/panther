import { Component, OnInit } from '@angular/core';
import { HeaderService } from './header.service';
import { MatDialog } from '@angular/material';
import { LoginService, User } from '@core';
import { Router } from '@angular/router';
import { AppUserComponent } from 'app/user';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class AppHeaderComponent implements OnInit {
    isAdmin: boolean;
    cookieName: string;
    constructor(
        private _header: HeaderService,
        private dialog: MatDialog,
        private data: LoginService,
        private route: Router,
        private cookie: CookieService,
    ) {}

    public get header(): string {
        return this._header.title;
    }

    ngOnInit() {
        const cookie = this.data.isLoggedIn;
        if (cookie && JSON.stringify(cookie) !== '{}') {
            for (const key in cookie) {
                if (!!key) {
                    if (key === 'admin') {
                        this.isAdmin = true;
                    }
                    this.cookieName = key;
                }
            }
        }
    }
    manage() {
        this.route.navigate(['/userlist']);
    }
    logout() {
        this.cookie.delete(this.cookieName, '/');
        this.route.navigate(['overview']);
        location.reload();
    }
    updatePasswd() {
        const user: User = {};
        user.name = this.cookieName;

        this.dialog.open(AppUserComponent, {
            width: '360px',
            data: user,
        });
    }
}

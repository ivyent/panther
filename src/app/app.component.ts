import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppLoginComponent } from './login';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    constructor(
        private dialog: MatDialog
    ) {}
    ngOnInit() {
        // const dialogref = this.dialog.open(AppLoginComponent, {});
    }
    ngOnDestroy() { }


}

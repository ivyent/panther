import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { User, LoginService } from '@core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {  MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { deepCopy } from '@core/utils';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})

export class AppUserComponent implements OnInit, OnDestroy {
    ob: User = {};
    form: FormGroup;
    formErrors: {
        [key: string]: any;
    };

    namedisabled: boolean;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        public dialogRef: MatDialogRef<AppUserComponent>,
        private user: LoginService,
    ) {}
    ngOnInit() {
        this.form = new FormGroup({
            name: new FormControl(this.ob.name, [Validators.required, Validators.min(1), Validators.max(12)]),
            password: new FormControl(this.ob.password, [Validators.required, Validators.min(1), Validators.max(12)]),
        });
        if (this.data) {
            this.ob = deepCopy(this.data);
            this.namedisabled = true;
        }
    }
    ngOnDestroy() {}

    do() {
        if (this.namedisabled) {
            this.user.update(this.ob).subscribe((data) => {
                if (data.status === 1) {
                    alert('更改成功');
                } else {
                    alert('更改失败');
                }
                this.dialogRef.close();
            });
        } else {
            this.ob.userType = 'primary';
            console.log(this.ob);
            this.user.create(this.ob).subscribe((data) => {
                if (data.status === 1) {
                    alert('创建成功');
                } else {
                    alert('创建失败');
                }
                console.log(data);
                this.dialogRef.close();
            });
        }
    }

    close () {
        this.dialogRef.close();
    }
}

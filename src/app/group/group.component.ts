import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HeaderService } from 'app/header';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { SystemScan, ExecService, ExecWebsocket, Order, Command, LoginService } from '@core';
import { Subscription } from 'rxjs';
import { deepCopy, sortingDataAccessor } from '@core/utils';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AppExecCmdComponent } from 'app/exec';

@Component({
    selector: 'app-group',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.scss']
})

export class AppGroupComponent implements OnInit, OnDestroy {
    group: string;
    displayedColumns: string[] = ['select', 'metadata.name', 'state', 'critical_sec', 'important_sec', 'moderate_sec'];
    dataSource: MatTableDataSource<SystemScan>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    selection = new SelectionModel<SystemScan>(true, []);
    selectInitValue = 'install';
    package: string;
    order: Order;
    command: Command;
    name: string;
    scan: ExecWebsocket;
    scansubscription: Subscription;
    cmd: ExecWebsocket;
    cmdnsubscription: Subscription;

    constructor(
        private _header: HeaderService,
        private _data: ExecService,
        private _route: ActivatedRoute,
        private dialog: MatDialog,
        private cookie: LoginService,
        private router: Router,
    ) {
        this._route.paramMap.subscribe(
            (params: ParamMap) => {
                this.group = decodeURI(params.get('group'));
                if (this.group === 'nogroup') {
                    this._header.title = `主机组 - 未分组`;
                } else {
                    this._header.title = `主机组 - ${this.group}`;
                }
            }
        );
    }

    ngOnInit() {
        this.scan = this._data.forScan('*');
        this.scansubscription = this.scan.connect().subscribe((data) => {
            const cookie = this.cookie.isLoggedIn;
            let tableData = [];
            if ( cookie && JSON.stringify(cookie) !== '{}') {
                for (const key in cookie) {
                    if (key !== 'admin') {
                        for (const val of data) {
                            if (val.metadata.user === key) {
                                tableData.push(val);
                            }
                        }
                    } else {
                        tableData = deepCopy(data);
                    }
                }
            }
            this.dataSource = new MatTableDataSource(this.filterGroup(tableData));
            this.dataSource.sortingDataAccessor = sortingDataAccessor;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });
        this.cmd = this._data.forCmd();
        this.cmdnsubscription = this.cmd.connect().subscribe((data) => {
            console.log(data);
        });
    }
    ngOnDestroy() {}

    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    toggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }
    filterGroup(data) {
        const groupData = deepCopy(data);
        const arr = [];
        for (const v of groupData) {
            if (this.group === 'nogroup') {
                if (v.metadata.group === undefined) {
                    arr.push(v);
                }
            } else {
                if (v.metadata.group === this.group) {
                    arr.push(v);
                }
            }
        }
        return arr;
    }

    execCommand() {
        if (this.selectInitValue === 'list installed') {
            const name = this.selection.selected[0].metadata.name;
            this.router.navigate(['history'], {
                queryParams: {
                    host: name,
                    package: this.package,
                }
            });
            return;
        }
        const commandlist = [];
        for (const val of this.selection.selected) {
            this.name = val.metadata.name;
            this.command =  new Command(this.name, `yum ${this.selectInitValue} -y ${this.package}`);
            commandlist.push(this.command);
        }
        this.order = new Order(commandlist);
        this.selection.clear();
        this.package = '';
        const ref = this.dialog.open(
            AppExecCmdComponent, {
                disableClose: true,
                data: this.order
            });
        ref.afterClosed().subscribe(() => {
            // this.scan.send(new Order([new Command(this.name)]));
            this.scan.send(this.order);
        });
    }
}

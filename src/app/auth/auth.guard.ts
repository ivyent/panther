import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoginService, LoginStatus } from '@core';
import { MatDialog } from '@angular/material';
import { AppLoginComponent } from 'app/login';

@Injectable()
export class AppAuthGuard implements CanActivate {
    constructor(
       private data: LoginService,
       private dialog: MatDialog,
       private route: Router
    ) {}
    subscription: Subscription;
    isLogin: boolean;
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!!this.data.isLoggedIn && JSON.stringify(this.data.isLoggedIn) !== '{}') {
            // console.log(this.data.isLoggedIn);
            return true;
        } else {
            this.dialog.open(AppLoginComponent, {width: '360px;'}).afterClosed().subscribe(() => {
                this.route.navigate(['overview']);
                location.reload(); // 重新加载页面，否则header component中无法获取cookie
            });
            return false;
        }
    }
}

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HeaderService } from 'app/header';
import { ExecService, SystemScan, ExecWebsocket, Order, Command, GroupService, LoginService } from '@core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { AppEditGroupComponent } from './editgroup';
import { deepCopy, filterAny, sortingDataAccessor } from '@core/utils';

@Component({
    selector: 'app-exec-scan',
    templateUrl: './scan.component.html',
    styleUrls: ['./scan.component.scss'],
})
export class AppExecScanComponent implements OnInit, OnDestroy {

    displayedColumns: string[] = [
        'select', 'metadata.name', 'state', 'metadata.group',  'critical_sec', 'important_sec', 'moderate_sec', 'updated_at', 'view'
    ];
    dataSource: MatTableDataSource<SystemScan>;
    selection = new SelectionModel<SystemScan>(true, []);
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    private scan: ExecWebsocket;
    private scanSubscription: Subscription;
    private cmd: ExecWebsocket;
    private cmdSubscription: Subscription;

    constructor(
        private _header: HeaderService,
        private exec: ExecService,
        public dialog: MatDialog,
        private _group: GroupService,
        private cookie: LoginService,
    ) {
        this.dataSource = new MatTableDataSource([]);
    }

    ngOnInit() {
        this._header.title = '安全漏洞';
        this.scan = this.exec.forScan('*');
        this.scanSubscription = this.scan.connect().subscribe((data: SystemScan[]) => {
            const cookie = this.cookie.isLoggedIn;
            let tableData = [];
            if ( cookie && JSON.stringify(cookie) !== '{}') {
                for (const key in cookie) {
                    if (key !== 'admin') {
                        for (const val of data) {
                            if (val.metadata.user === key) {
                                tableData.push(val);
                            }
                        }
                    } else {
                        tableData = deepCopy(data);
                    }
                }
            }
            this.dataSource.data = tableData;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sortingDataAccessor = sortingDataAccessor;
            this.dataSource.filterPredicate = filterAny;
            this.dataSource.sort = this.sort;
        });
        this.cmd = this.exec.forCmd();
        this.cmdSubscription = this.cmd.connect().subscribe((data: any) => {
            console.log(data);
        });
    }

    ngOnDestroy() {
        this.scanSubscription.unsubscribe();
        this.cmdSubscription.unsubscribe();
        this.scan.disconnect();
        this.cmd.disconnect();
    }

    search(value: string) {
        this.dataSource.filter = value.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    toggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    rescan() {
        const cmds: Command[] = [];
        for (const each of this.selection.selected) {
            cmds.push(new Command(each.metadata.name));
        }
        this.scan.send(new Order(cmds));
        this.selection.clear();
    }
// 指定分组
    assign() {
        const dialogRef = this.dialog.open(AppEditGroupComponent, {
            data: this.selection.selected,
            disableClose: true,
        });
        dialogRef.afterClosed().subscribe(() => {
            this.selection.clear();
            this.scanSubscription = this.scan.connect().subscribe((data: SystemScan[]) => {
                this.dataSource.data = data;
            });
        });
    }
// 移除分组
    remove() {
        const array = [];
        for (const each of this.selection.selected) {
            array.push(each.metadata.name);
        }
        array.push('');
        this._group.update(array).subscribe(() => {
            this.selection.clear();
        });
    }

    getTotal(num) {
         return this.dataSource.data.map((x) => {
            if (!!x.security) {
                let total = 0;
                for (const value of x.security) {
                    if (value.severity === num) {
                        total++;
                    }
                }
                return total;
            } else {
                return 0;
            }
        }).reduce((x, y) => {
            return x + y;
        });
    }
}

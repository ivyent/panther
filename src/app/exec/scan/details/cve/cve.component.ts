import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeaderService } from 'app/header';
import { CveService, CVEEntity } from '@core';
import { deepCopy } from '@core/utils';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-cve',
    templateUrl: './cve.component.html',
    styleUrls: ['./cve.component.scss']
})

export class AppCveComponent implements OnInit, OnDestroy {
    name: string;
    data: CVEEntity;
    subscription: Subscription;
    constructor(
        private route: ActivatedRoute,
        private _header: HeaderService,
        private cve: CveService
    ) {}
    ngOnInit() {
        this.route.params.subscribe(queryParams => {
            this.name = queryParams.name;
        });
        this._header.title =  this.name;
        this.subscription = this.cve.fetch([this.name]).subscribe((data) => {
            this.data = deepCopy(data);
            console.log(this.data);
        });

    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}

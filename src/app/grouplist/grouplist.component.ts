import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HeaderService } from 'app/header';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { SystemScan, ExecService, ExecWebsocket, LoginService } from '@core';
import { Subscription } from 'rxjs';
import { deepCopy } from '@core/utils';

@Component({
    selector: 'app-group-list',
    templateUrl: './grouplist.component.html',
    styleUrls: ['./grouplist.component.scss']
})

export class AppGroupListComponent implements OnInit, OnDestroy {

    displayedColumns = ['group', 'host_count', 'critical_sec', 'important_sec', 'moderate_sec', 'view'];
    classtifiedData: any[];
    private scan: ExecWebsocket;
    dataSource: MatTableDataSource<GroupList>;
    private scanSubscription: Subscription;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(
        private _header: HeaderService,
        private exec: ExecService,
        public dialog: MatDialog,
        private cookie: LoginService,
    ) {
        this.dataSource = new MatTableDataSource();
    }
    ngOnInit() {
        this._header.title = '主机组管理';
        this.scan = this.exec.forScan('*');
        this.scanSubscription = this.scan.connect().subscribe((data: SystemScan[]) => {
            const cookie = this.cookie.isLoggedIn;
            let tableData = [];
            if ( cookie && JSON.stringify(cookie) !== '{}') {
                for (const key in cookie) {
                    if (key !== 'admin') {
                        for (const val of data) {
                            if (val.metadata.user === key) {
                                tableData.push(val);
                            }
                        }
                    } else {
                        tableData = deepCopy(data);
                    }
                }
            }
            console.log(data);
            this.dataSource.data = this.classify(tableData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });

    }
    ngOnDestroy() {
        this.scanSubscription.unsubscribe();
    }

    search(value: string) {
        this.dataSource.filter = value.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    // 以group进行分组
    classify(data) {
        const copyData = deepCopy(data);
        const array = [];
        array['nogroup'] = [];
        for (const each of copyData) {
            if (each.metadata.group === undefined) {
                array['nogroup'].push(each);
            } else {
                if (array[each.metadata.group] === undefined) {
                    array[each.metadata.group] = [];
                    array[each.metadata.group].push(each);
                } else {
                    array[each.metadata.group].push(each);
                }
            }
        }
        if (array['nogroup'].length === 0) {
            delete array['nogroup'];
        }
        const tabledata = [];
        for (const key in array) {
            if (!!array[key]) {
                const obj = {};
                obj['group'] = key;
                obj['host_count'] = array[key].length;
                let criticalTotalCount = 0;
                let importantTotalCount = 0;
                let moderateTotalCount = 0;
                for (const each of array[key]) {
                    let critical_sec = 0;
                    let important_sec = 0;
                    let moderate_sec = 0;
                    if (!!each.security) {
                        for (const val of each.security) {
                            switch (val.severity) {
                                case 1:
                                    critical_sec++;
                                break;
                                case 2:
                                    important_sec++;
                                break;
                                case 3:
                                    moderate_sec++;
                                break;
                            }
                        }
                    }
                    criticalTotalCount += critical_sec;
                    importantTotalCount += important_sec;
                    moderateTotalCount += moderate_sec;
                }
                obj['critical_sec'] = criticalTotalCount;
                obj['important_sec'] = importantTotalCount;
                obj['moderate_sec'] = moderateTotalCount;
                tabledata.push(obj);
            }

        }

        return tabledata;
    }

}
export class GroupList  {
    group: string;
    host_count: number;
    critical_sec: number;
    important_sec: number;
    moderate_sec: number;

}


import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService, User } from '@core';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class AppLoginComponent implements OnInit, OnDestroy {
    loginType = [
        {value: 'admin', viewValue: '管理员'},
        {value: 'primary', viewValue: '普通用户'},
    ];
    ob: User = {};
    constructor(
        private _data: LoginService,
        public dialog: MatDialogRef<AppLoginComponent>
    ) {}
    subscription: Subscription;
    ngOnInit() {}
    ngOnDestroy() {

    }

    login() {
        console.log(this.ob);
        this._data.login(this.ob).subscribe((data) => {
            console.log(data);
            this.dialog.close();
        });
    }
}


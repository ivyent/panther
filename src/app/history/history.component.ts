import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { HistoryService } from '@core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderService } from 'app/header';

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.scss']
})
export class AppHistoryComponent implements OnInit, OnDestroy {
    host: string;
    package: string;
    displayedColumns: string[] = [ 'package', 'version', 'created_at', 'operation'];
    dataSource: MatTableDataSource<PackageInfomation>;

    @ViewChild(MatPaginator) paginator: MatPaginator;


    subscription: Subscription;
    constructor(
        private _hist: HistoryService,
        private router: ActivatedRoute,
        private _header: HeaderService,
    ) {}
    ngOnInit() {
        const host = this.router.queryParams.subscribe((params) => {
            this.host = params.host;
            this.package = params.package;
            this._header.title = `${this.host} -- ${this.package}`;
        });
        if (!!this.host) {
            this.subscription = this._hist.fetch(this.host).subscribe((data: any) => {
                console.log(data);
                this.dataSource = new MatTableDataSource(data.software);
                this.dataSource.paginator = this.paginator;
                this.dataSource.filter = this.package.trim().toLowerCase();
            });
        }
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    search(value: string) {
        this.dataSource.filter = value.trim().toLowerCase();
        this._header.title = `${this.host} -- ${value}`;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
}

export interface PackageInfomation {
    package: string;
    version: string;
    operation: string;
    created_at: string;
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-import',
    templateUrl: './import.component.html',
    styleUrls: ['./import.component.scss']
})

export class AppImportComponent implements OnInit, OnDestroy {
    IsSubmit = true;
    constructor(
        public dialogRef: MatDialogRef<AppImportComponent>
    ) {}
    ngOnInit() {}
    ngOnDestroy() {}
    submit() {
        // e.preventDefault();
        if (this.IsSubmit) {
            const form = document.getElementById('form');
            (form as HTMLFormElement).submit();
            this.IsSubmit = false;
            this.dialogRef.close();
            return false;
        }
    }

    close() {
        this.dialogRef.close();
    }
}

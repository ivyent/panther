import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HeaderService } from '../header';
import { Host, HostService, Order, Command, ExecWebsocket, LoginService } from '@core';
import { Subscription, Observable } from 'rxjs';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { AppHostEditComponent } from './edit';
import { AppHostConfirmComponent } from './confirm';
import { AppImportComponent } from './import';
import { sortingDataAccessor, deepCopy, filterAny } from '@core/utils';
import { AppExecCmdComponent } from 'app/exec';
import { Router } from '@angular/router';

@Component({
    selector: 'app-host',
    templateUrl: './host.component.html',
    styleUrls: ['./host.component.scss'],
    entryComponents: [
        AppHostEditComponent,
        AppHostConfirmComponent
    ]
})
export class AppHostComponent implements OnInit, OnDestroy {

    displayedColumns: string[] = [
        'select', 'metadata.name', 'ssh_addr', 'ssh_port', 'ssh_user', 'op_user',
        'comment', 'metadata.created_at', 'metadata.updated_at', 'view'
    ];
    dataSource: MatTableDataSource<Host>;
    selection = new SelectionModel<Host>(true, []);
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    private subscription: Subscription;

    selectInitValue = 'install';
    package: string;
    order: Order;
    command: Command;
    scan: ExecWebsocket;
    constructor(
        public dialog: MatDialog,
        private _header: HeaderService,
        private _data: HostService,
        private cookie: LoginService,
        private router: Router,
    ) {}

    ngOnInit() {
        this._header.title = '主机管理';
        this.subscription = this._data.fetch('*').subscribe((data) => {
            const cookie = this.cookie.isLoggedIn;
            let tableData = [];
            if ( cookie && JSON.stringify(cookie) !== '{}') {
                for (const key in cookie) {
                    if (key !== 'admin') {
                        for (const val of data) {
                            if (val.metadata.user === key) {
                                tableData.push(val);
                            }
                        }
                    } else {
                        tableData = deepCopy(data);
                    }
                }
            }
            this.dataSource = new MatTableDataSource(tableData ? tableData : []);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sortingDataAccessor = sortingDataAccessor;
            this.dataSource.sort = this.sort;
            this.dataSource.filterPredicate = filterAny;
        });
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    search(value: string) {
        this.dataSource.filter = value.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    toggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    create() {
        const ref = this.dialog.open(AppHostEditComponent, {disableClose: true, data: null});
        ref.afterClosed().subscribe((obj) => {
            if (!obj) {
                return;
            }
            this._data.create(obj).subscribe((data) => {
                // MatTableDataSource's data is indeed a callback fn, so we must copy it first, and then
                // assign its value with the updated one.
                const dump = this.dataSource.data;
                dump.push(data);
                this.dataSource.data = dump;
            }, (err) => {
                // TODO: ERROR SHALL BE PROMPT ONTO SCREEN!
                console.error(err);
            });
        });
    }

    update(obj: Host) {
        const ref = this.dialog.open(AppHostEditComponent, {disableClose: true, data: obj});
        ref.afterClosed().subscribe((updated) => {
            if (!updated) {
                return;
            }
            this._data.update(updated).subscribe((data) => {
                if (!obj) {
                    return;
                }
                for (const i in this.dataSource.data) {
                    if (this.dataSource.data[i].metadata.name === data.metadata.name) {
                        const dump = this.dataSource.data;
                        dump[i] = data;
                        this.dataSource.data = dump;
                        break;
                    }
                }
            }, (err) => {
                // TODO: ERROR SHALL BE PROMPT ONTO SCREEN!
                console.error(err);
            });
        });
    }

    deleteSelection() {
        const ref = this.dialog.open(AppHostConfirmComponent, {disableClose: true, data: this.selection.selected});
        ref.afterClosed().subscribe((agreed) => {
            if (!agreed) {
                return;
            }
            const recur = (fn: (number) => Observable<void>, length: number) => {
                const index = length - 1;
                if (index < 0) {
                    this.selection.clear();
                    this.refresh();
                    return;
                }
                fn(length - 1).subscribe(() => {
                    recur(fn, index);
                }, (err) => {
                    console.error(err);
                });
            };
            // Delete machine recursively.
            recur((index: number) => {
                return this._data.delete(this.selection.selected[index].metadata.name);
            }, this.selection.selected.length);
        });
    }

    delete(obj: Host) {
        const ref = this.dialog.open(AppHostConfirmComponent, {disableClose: true, data: [obj]});
        ref.afterClosed().subscribe((agreed) => {
            if (!agreed) {
                return;
            }
            const name = obj.metadata.name;
            this._data.delete(name).subscribe(() => {
                for (const i in this.dataSource.data) {
                    if (this.dataSource.data[i].metadata.name === name) {
                        const dump = this.dataSource.data;
                        dump.splice(Number(i), 1);
                        this.dataSource.data = dump;
                        break;
                    }
                }
            }, (err) => {
                // TODO: ERROR SHALL BE PROMPT ONTO SCREEN!
                console.error(err);
            });
        });
    }

    refresh() {
        if (this.subscription) {
            this.subscription.unsubscribe(); // GC previous thread.
            this.subscription = null;
        }
        this.subscription = this._data.fetch('*').subscribe((data) => {
            if (this.dataSource) {
                let tableData = [];
                const cookie = this.cookie.isLoggedIn;
                if ( cookie && JSON.stringify(cookie) !== '{}') {
                    for (const key in cookie) {
                        if (key !== 'admin') {
                            for (const val of data) {
                                if (val.metadata.user === key) {
                                    tableData.push(val);
                                }
                            }
                        } else {
                            tableData = deepCopy(data);
                        }
                    }
                }
                this.dataSource.data = tableData;
                return;
            }
            this.dataSource = new MatTableDataSource(data);
        });
    }
    // 导出一个csv表格模板
    export() {
        const header =
            ' Sequence Number, Host name, SSH Address, SSH Port, SSH User, SSH Password, Administrator, Password, Comment, Platform account';
        const example =
            '1, server, 192.168.254.128, 22, root, vagrant, test, vagrant, server, test3';
        const csv = 'data:text/csv;charset=utf-8,\ufeff' + header + '\n' + example;
        const link = document.createElement('a');
        link.setAttribute('href', csv);
        link.setAttribute('download', 'hostlist.csv');
        link.click();
    }
    import() {
        const dialog = this.dialog.open(AppImportComponent, {
            disableClose: true,
        });
        dialog.afterClosed().subscribe(() => {
            this.selection.clear();
            this.subscription = this._data.fetch('*').subscribe((data) => {
                this.dataSource = new MatTableDataSource(data ? data : []);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sortingDataAccessor = sortingDataAccessor;
                this.dataSource.sort = this.sort;
            });
        });
    }


    execCommand() {
        if (this.selectInitValue === 'list installed') {
            const name = this.selection.selected[0].metadata.name;
            this.router.navigate(['history'], {
                queryParams: {
                    host: name,
                    package: this.package,
                }
            });
            return;
        }
        const commandlist = [];
        for (const val of this.selection.selected) {
            this.command =  new Command(val.metadata.name, `yum ${this.selectInitValue} -y ${this.package}`);
            commandlist.push(this.command);
        }
        this.order = new Order(commandlist);
        this.selection.clear();
        this.package = '';
        const ref = this.dialog.open(
            AppExecCmdComponent, {
                disableClose: true,
                data: this.order
            });
        ref.afterClosed().subscribe(() => {
            // this.scan.send(new Order([new Command(this.name)]));
            // this.scan.send(this.order);
        });
    }
}

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';
import { TextFieldModule } from '@angular/cdk/text-field';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppHeaderComponent, HeaderService } from './header';
import { CoreModule } from '@core';
import { AppHostComponent, AppHostConfirmComponent, AppHostEditComponent, AppImportComponent } from './host';
import { AppExecScanComponent, AppExecScanDetailsComponent, AppExecCmdComponent, AppEditGroupComponent, AppCveComponent } from './exec';
import { AppGroupListComponent } from './grouplist';
import { AppGroupComponent } from './group';
import { AppLoginComponent } from './login';
import { AppAuthGuard } from './auth/auth.guard';
import { CookieService } from 'ngx-cookie-service';
import { AppUserComponent } from './user';
import { AppUserListComponent } from './userlist/userlist.component';
import { AppOverviewComponent } from './overview';
import { AppHistoryComponent } from './history';

@NgModule({
    declarations: [
        AppComponent,
        AppHeaderComponent,
        AppExecScanComponent,
        AppExecScanDetailsComponent,
        AppHostComponent,
        AppHostConfirmComponent,
        AppHostEditComponent,
        AppExecCmdComponent,
        AppGroupListComponent,
        AppGroupComponent,
        AppEditGroupComponent,
        AppImportComponent,
        AppLoginComponent,
        AppUserComponent,
        AppUserListComponent,
        AppOverviewComponent,
        AppCveComponent,
        AppHistoryComponent
    ],
    entryComponents: [
        AppHostConfirmComponent,
        AppHostEditComponent,
        AppExecCmdComponent,
        AppEditGroupComponent,
        AppImportComponent,
        AppLoginComponent,
        AppUserComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        CdkTableModule,
        TextFieldModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatStepperModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
        FlexLayoutModule,
        AppRoutingModule,
        CoreModule,
    ],
    providers: [
        HeaderService,
        AppAuthGuard,
        CookieService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

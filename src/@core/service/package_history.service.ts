import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const hist_api = '/api/v1/history';
@Injectable({
    providedIn: 'root'
})

export class HistoryService {
    constructor(
        private _http: HttpClient
    ) {}

    fetch(target): Observable<void> {
        let params = new HttpParams();
            params = params.set('search', target);
        return this._http.get<void>(hist_api, {params: params});
    }
}

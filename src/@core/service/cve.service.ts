import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CVEEntity } from './types';
const cve_api = '/api/v1/cve';
@Injectable({
    providedIn: 'root'
})

export class CveService {
    constructor(
        private _http: HttpClient
    ) {}


    fetch(target: '*'|string[]) {
        let params = new HttpParams();
        if (target === '*') {
            params = params.set('search', '*');
        } else {
            params = params.set('search', target.join(','));
        }
        return this._http.get<CVEEntity[]>(cve_api, {params: params});
    }
}
